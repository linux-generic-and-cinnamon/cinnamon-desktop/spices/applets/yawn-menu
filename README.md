YAWN Menu is a fork of the official Mint 19.2 Cinnamon menu applet to which a few updates and improvements have been applied.       
Original improvement is the ability to select which launchers area on which panel to add the selected application launcher to.		

Official menus only allow adding launchers to first/main panel only, and also only to first launchers area on that panel. However, a system can have more than one panel, and on each panel more than one launchers area. So until now there was a severe limitation in regard to where would a launcher go when in menu the user would right-click an application and select **Add to panel**.

This fork adds a complete list of all panels and launcher areas in the system, so the user can immediately choose the desired location:
![](menu_options.png) 

Panel number and position on screen are first indication, and in paranthesis there's the index of the launchers area, so when more such areas are located on same panel the user will know which one to choose based on its index. This should obsolete any subsequent steps of moving the launcher(s) from one panel/area to another.

Additions from the 5.8 menu are the options for setting icon sizes, adding application actions to the context menu, as well as a few minor internal changes.

**© Drugwash, 2023**
